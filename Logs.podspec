

Pod::Spec.new do |spec|

spec.name         = "Logs"
spec.version      = "0.0.1"
spec.summary      = "Logs"

spec.description  = <<-DESC
  Logs library
                   DESC

spec.homepage     = "http://github.com"

spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }
spec.author       = { "sergeymild" => "jean.timex@gmail.com" }
spec.source       = { git: "https://sergeymild@bitbucket.org/sergeymild/swiftlogs.git", tag: "master" }
spec.swift_version = "5.1"
spec.ios.deployment_target = "11.4"
spec.source_files  = "Logs/**/*.{h,m,swift}"
end
