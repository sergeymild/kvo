//
//  ViewController.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 24/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import UIKit

import YandexSdk
import BMPlayer
import AudioPlayer

struct AppDirectory {
    static let localDocumentsURL = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: .userDomainMask).last!
    
    fileprivate static let downloadsFolderURL = localDocumentsURL//.appendingPathComponent("Downloads")
    static func getDownloadsFolderURL() -> URL {
        if !FileManager.default.fileExists(atPath: downloadsFolderURL.path) {
            try! FileManager.default.createDirectory(atPath: downloadsFolderURL.path, withIntermediateDirectories: true, attributes: nil)
        }
        return downloadsFolderURL
    }
}

func getWindow() -> UIWindow? {
    if let applicationDelegate = UIApplication.shared.delegate as? AppDelegate,
        let window = applicationDelegate.window {
        return window
    }
    return nil
}

class ViewController: UIViewController {
    let url = URL(string: "https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_30mb.mp4")!
    let player = BMPlayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        let playlist = [
            AudioModel(
                path: AppDirectory.getDownloadsFolderURL().appendingPathComponent("Nadia Ali - Rapture (Avicii Remix) [Official Music Video].mp3").path,
                title: "Nadia Ali - Rapture (Avicii Remix) [Official Music Video]",
                subtitle: "4:43",
                durationInSeconds: 4 * 60 + 45
            ),
            
            AudioModel(
                path: AppDirectory.getDownloadsFolderURL().appendingPathComponent("Chris Lake - Stay With Me.mp3").path,
                title: "Chris Lake - Stay With Me",
                subtitle: "3:45",
                durationInSeconds: 3 * 60 + 45
            ),
            
            AudioModel(
                path: AppDirectory.getDownloadsFolderURL().appendingPathComponent("Chris Lake & Solardo - Free Your Body.mp3").path,
                title: "Chris Lake & Solardo - Free Your Body.mp3",
                subtitle: "4:30",
                durationInSeconds: 4 * 60 + 30)
        ]
        
        
        NotificationCenter.default.addObserver(forName: .didShowMainPlayerClick, object: nil, queue: .main) { _ in
            
            //dismissMiniPlayerView(window: self.view)
            let controller = AudioPlayerViewController(
                playlist: playlist,
                restartPlay: false,
                miniplayerParentView: self.view
            )
            
            self.present(controller, animated: true)
        }
        
        
        let controller = AudioPlayerViewController(
            playlist: playlist,
            miniplayerParentView: view
        )
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.present(controller, animated: true)
        }
        
        
        
//        view.addSubview(player)
//        player.frame = .init(x: 0, y: 0, width: view.frame.width, height: 200)
//        player.setVideo(resource: .init(url: url))
//        player.play()
//
//        player.backBlock = { [unowned self] (isFullScreen) in
//            if isFullScreen == true { return }
//            let _ = self.navigationController?.popViewController(animated: true)
//        }
    }


}

