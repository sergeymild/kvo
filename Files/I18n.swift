//
//  I18n.swift
//  Files
//
//  Created by Sergei Golishnikov on 24/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import Foundation

internal enum L10n {
    internal enum Files {
        /// No files
        internal static let noFiles = L10n.tr("Localizable", "files.noFiles")
        /// %ld File(s) %@
        internal static func listItemFolderSubtitle(_ p1: Int, _ p2: String) -> String {
          return L10n.tr("Localizable", "files.listItemFolderSubtitle", p1, p2)
        }
        /// %@ %@
        internal static func simpleSubtitle(_ p1: String, _ p2: String) -> String {
          return L10n.tr("Localizable", "files.simpleSubtitle", p1, p2)
        }
        /// %@ / %@
        internal static func mediaSubtitle(_ p1: String, _ p2: String) -> String {
          return L10n.tr("Localizable", "files.mediaSubtitle", p1, p2)
        }
    }
}

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
