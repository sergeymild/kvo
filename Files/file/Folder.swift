//
//  Folder.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 01/03/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation

public protocol FolderProvider {
    func getFolders(parent: String) -> [Folder]
}

public struct Folder {
    let name: String
    let absolutePath: String
    let isHasSubfolders: Bool
}
