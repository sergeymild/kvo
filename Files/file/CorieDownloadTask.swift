//
//  DownloadTask.swift
//  Corie
//
//  Created by Sergei Golishnikov on 09/12/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation

enum CorieDownloadState: String {
    case none
    case downloading
    case preparing
    case processing
    case compressing
    case decompressing
    case queued
    case paused
    case failed
    case complete
}

protocol CorieDownloadTask {
    var id: String { get }
    var parentFolder: String { get }
    var filename: String { get }
    var sessionTask: URLSessionTask? { get set }
    var link: String { get }
    var progress: Float { get set }
    var taskIdentifier: Int { get }
    var state: CorieDownloadState { get set }
    var total: Int64 { get set }
    var completed: Int64 { get set }
    var error: String? { get set }
    var isUpload: Bool { get }
    func didFinishDownload(to location: URL)
    
    func didProgressChange(total: Double, completed: Double)
    
    func pause(callback: @escaping () -> Void)
    func formatProgressString() -> String
    
    func createOperation() -> Operation
}

extension CorieDownloadTask {
    var taskIdentifier: Int {
        sessionTask?.taskIdentifier ?? -1
    }
    
    var isUpload: Bool {
        return false
    }
}
