//
//  FileMediaTypeRetriever.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 01/03/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation
import AVKit

public protocol MediaPositionProvider {
    func readPosition(path: String, type: FileContentType) -> TimeInterval
}

public class DummyMediaPositionProvider: MediaPositionProvider {
    public init() {}
    public func readPosition(path: String, type: FileContentType) -> TimeInterval {
        return 0
    }
}

public protocol MediaTypeRetriever {
    func retrieveDurationInSeconds(path: String) -> Double
    func retrieveMediaInformation(path: String, type: FileContentType) -> MediaInformation
}

public func formatDuration(duration: Float64) -> String {
    let hours = Int(duration / 3600)
    let minutes = Int(duration.truncatingRemainder(dividingBy: 3600) / 60)
    let seconds = Int(duration.truncatingRemainder(dividingBy: 60))
    
    var formatted: String = ""
    if hours > 0 {
        formatted = String(format: "%i:%02i:%02i", hours, minutes, seconds)
    } else {
        formatted = String(format: "%02i:%02i", minutes, seconds)
    }
    return formatted
}

public class FileMediaTypeRetriever: MediaTypeRetriever {
    fileprivate let allowedForAssetRetrieveExtensions = ["avi", "mkv"]
    fileprivate var durationCache: [String: String] = [:]
    fileprivate var metadataCache: [String: (String?, String?)] = [:]
    
    fileprivate var mediaCache: [String: MediaInformation] = [:]
    let mediaPositionProvider: MediaPositionProvider
    
    public init(
        mediaPositionProvider: MediaPositionProvider = DummyMediaPositionProvider()
    ) {
        self.mediaPositionProvider = mediaPositionProvider
    }
    
    func retrieveAuthorTrackTitle(path: String) -> (String?, String?) {
        if let metadata = metadataCache[path] { return metadata }
        var title: String? = nil
        var artist: String? = nil
        
        let commonMetadata = AVAsset(url: URL(fileURLWithPath: path)).commonMetadata
        let titles = AVMetadataItem.metadataItems(from: commonMetadata, withKey: AVMetadataKey.commonKeyTitle, keySpace: AVMetadataKeySpace.common)
        let artists = AVMetadataItem.metadataItems(from: commonMetadata, withKey: AVMetadataKey.commonKeyArtist, keySpace: AVMetadataKeySpace.common)
        artist = artists.first?.value as? String
        title = titles.first?.value as? String
        let metadata = (title, artist)
        metadataCache[path] = metadata
        return metadata
    }
    
    public func retrieveDurationInSeconds(path: String) -> Double {
        let asset = AVAsset(url: URL(fileURLWithPath: path))
        let duration = asset.duration
        return CMTimeGetSeconds(duration)
    }
    
    func retrieveDuration(path: String) -> String {
        if let formatted = durationCache[path] { return formatted }
        let asset = AVAsset(url: URL(fileURLWithPath: path))
        let duration = asset.duration
        let durationTime = CMTimeGetSeconds(duration)
        let formatted: String = formatDuration(duration: durationTime)
        durationCache[path] = formatted
        return formatted
    }
    
    public func retrieveMediaInformation(
        path: String,
        type: FileContentType
    ) -> MediaInformation {
        if let cache = mediaCache[path] { return cache }
        
        let asset = AVAsset(url: URL(fileURLWithPath: path))
        
        let commonMetadata = asset.commonMetadata
        let titles = AVMetadataItem.metadataItems(from: commonMetadata, withKey: AVMetadataKey.commonKeyTitle, keySpace: AVMetadataKeySpace.common)
        let artists = AVMetadataItem.metadataItems(from: commonMetadata, withKey: AVMetadataKey.commonKeyArtist, keySpace: AVMetadataKeySpace.common)

        let seconds = CMTimeGetSeconds(asset.duration)
        let bookmark = seconds > 600 ? mediaPositionProvider.readPosition(path: path, type: type) : 0
        let artist = artists.first?.value as? String
        let title = titles.first?.value as? String
        let media = MediaInformation(
            durationInSeconds: seconds,
            bookmark: bookmark >= seconds ? 0 : bookmark,
            formattedDuration: formatDuration(duration: seconds),
            title: title != nil && artist != nil ? "\(artist!) - \(title!)" : path.nameWithoutExtension
        )
        mediaCache[path] = media
        return media
    }
}
