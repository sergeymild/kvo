//
//  SortType.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 05/10/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import Foundation

public enum SortType: String {
    case name_asc
    case name_desc
    case size_asc
    case size_desc
    case date_asc
    case date_desc
    
    public func toggle() -> SortType {
        switch self {
        case .name_asc:
            return .name_desc
        case .name_desc:
            return .name_asc
        case .size_asc:
            return .size_desc
        case .size_desc:
            return .size_asc
        case .date_asc:
            return .date_desc
        case .date_desc:
            return .date_asc
        }
    }
    
    public func toggleIfTheSame(type: SortType) -> SortType {
        if isTheSame(type: type) {
            return toggle()
        }
        return type
    }
    
    public func isTheSame(type: SortType) -> Bool {
        return self.normalize() == type.normalize()
    }
    
    public func isTheSameType(type: SortType) -> Bool {
        return self.normalize()
            .replacingOccurrences(of: "_asc", with: "")
            .replacingOccurrences(of: "_desc", with: "") ==
            type.normalize()
                .replacingOccurrences(of: "_asc", with: "")
                .replacingOccurrences(of: "_desc", with: "")
    }
    
    fileprivate func normalize() -> String {
        return self.rawValue.replacingOccurrences(of: "_desc", with: "").replacingOccurrences(of: "_asc", with: "")
    }
}
