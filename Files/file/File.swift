//
//  Resource.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 19/02/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

//enum FileType {
//    case directory
//    case dropbox
//    case image
//    case audio
//    case video
//    case file
//}

public enum FileContentType {
    case image
    case audio
    case video
    case file
    case zip
    case doc
    case pdf
}

public enum FileType {
    case file
    case directory
    case dropbox
    case yandexDisk
    case iCloud
}

public enum EditAction: CaseIterable {
    case delete
    case rename
    case move
}

public struct MediaInformation {
    let durationInSeconds: Double
    var bookmark: TimeInterval
    let formattedDuration: String
    let title: String?
}

public class File: CustomStringConvertible {
    var name: String
    var path: String
    var lastModificationTime: Date
    var subtitle: String = ""
    var formattedDuration: String = ""
    var size: Int64 = 0
    var contentType: FileContentType
    var fileType: FileType
    var ext: String
    var childCount: Int = 0
    var downloadItem: CorieDownloadTask? = nil
    public var description: String { path }
    let editActions: [EditAction]
    var mediaInformation: MediaInformation?
    var preview: String?

    func replace(file: File) {
        self.name = file.name
        self.path = file.path
        self.lastModificationTime = file.lastModificationTime
        //self.absolutePath = file.absolutePath
        self.subtitle = file.subtitle
        self.formattedDuration = file.formattedDuration
        self.size = file.size
        self.fileType = file.fileType
        self.contentType = file.contentType
        self.ext = file.ext
        self.childCount = file.childCount
        self.downloadItem = file.downloadItem
    }
    
    init(
        name: String,
        path: String,
        lastModificationTime: Date = Date(),
        //absolutePath: String,
        size: Int64,
        fileType: FileType,
        contentType: FileContentType = .file,
        ext: String,
        childCount: Int = 0,
        description: String = "",
        downloadItem: CorieDownloadTask? = nil,
        editActions: [EditAction] = EditAction.allCases
    ) {
        self.name = name
        self.path = path
        self.lastModificationTime = lastModificationTime
        //self.absolutePath = absolutePath
        self.size = size
        self.fileType = fileType
        self.contentType = contentType
        self.subtitle = description
        self.ext = ext
        self.childCount = childCount
        self.downloadItem = downloadItem
        self.editActions = editActions
    }
}
