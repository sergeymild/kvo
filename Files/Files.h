//
//  Files.h
//  Files
//
//  Created by Sergei Golishnikov on 24/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Files.
FOUNDATION_EXPORT double FilesVersionNumber;

//! Project version string for Files.
FOUNDATION_EXPORT const unsigned char FilesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Files/PublicHeader.h>


#import "FileNativeWrapper.h"
