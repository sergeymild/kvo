# swiftgen strings \
# 	ConnectClub/assets/en.lproj/Localizable.strings \
# 	--output ConnectClub/assets/Localized.generated.swift \
# 	--templatePath ./flat-swift4.stencil

swiftgen  xcassets \
	AudioPlayer/Images.xcassets \
	AudioPlayer/Colors.xcassets \
	--output AudioPlayer/Assets.swift \
	--templatePath ./templ.stencil