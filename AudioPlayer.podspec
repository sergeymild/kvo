Pod::Spec.new do |spec|

spec.name         = "AudioPlayer"
spec.version      = "0.0.1"
spec.summary      = "AudioPlayer"

spec.description  = <<-DESC
  AudioPlayer library
                   DESC

spec.static_framework = true
spec.homepage     = "http://github.com"

spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }
spec.author       = { "sergeymild" => "jean.timex@gmail.com" }
spec.source       = { git: "https://sergeymild@bitbucket.org/sergeymild/swiftlogs.git", tag: "master" }
spec.swift_version = "5.1"
spec.ios.deployment_target = "11.4"
spec.source_files  = "AudioPlayer/**/*.{h,m,swift}"
spec.framework     = 'Logs'
spec.framework     = 'CommonExtensions'
spec.dependency    = "KVO"
end

