//
//  AudioModel.swift
//  AudioPlayer
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import Foundation
import UIKit

public struct AudioModel {
    public let path: String
    let title: String
    let subtitle: String
    let durationInSeconds: Double
    let thumbnail: UIImage?
    let bookmark: TimeInterval
    
    public init(
        path: String,
        title: String,
        subtitle: String,
        durationInSeconds: Double,
        bookmark: TimeInterval = 0,
        thumbnail: UIImage? = nil
    ) {
        self.path = path
        self.title = title
        self.subtitle = subtitle
        self.durationInSeconds = durationInSeconds
        self.bookmark = bookmark
        self.thumbnail = thumbnail
    }
}
