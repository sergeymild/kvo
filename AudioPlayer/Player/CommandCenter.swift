//
//  CommandCenter.swift
//  VKMusic
//
//  Created by Yaro
//

import Foundation
import MediaPlayer
import CommonExtensions

fileprivate func placeholderImage() -> UIImage {
    return Asset.Images.icAudio.image.withBackground(
        color: .white,
        size: .init(width: 120, height: 120)
    )!
}

fileprivate func resize(image: UIImage?, size: CGSize) -> UIImage {
    guard let image = image else {
        return placeholderImage()
    }
    return image.createScaled(size: size)
}

internal class CommandCenter: NSObject {
    private let artworkSize = CGSize(width: 120, height: 120)
    static let defaultCenter = CommandCenter()
    
    fileprivate let player = AudioPlayer.shared
    
    override init() {
        super.init()
        setCommandCenter()
        setAudioSeccion()
    }
    
    deinit { NotificationCenter.default.removeObserver(self) }
   
    func setAudioSeccion() { //TODO: Change to .defaultToSpeaker to show music controls on Locked Screen or .mixWithOthers not to show
        do { try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            do { try AVAudioSession.sharedInstance().setActive(true) }
            catch let error as NSError { print(error.localizedDescription) }
        }
        catch let error as NSError { print(error.localizedDescription) }
    }
    
    //MARK: - Remote Command Center
    fileprivate func setCommandCenter() {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.pauseCommand.isEnabled = true
        commandCenter.playCommand.isEnabled = true
        commandCenter.previousTrackCommand.isEnabled = true
        commandCenter.nextTrackCommand.isEnabled = true
        commandCenter.changePlaybackPositionCommand.isEnabled = true
        
        commandCenter.changePlaybackPositionCommand.addTarget(self, action: #selector(changePlayback))
        commandCenter.pauseCommand.addTarget(self, action: #selector(remoteCommandPause))
        commandCenter.playCommand.addTarget(self, action: #selector(remoteCommandPlay))
        commandCenter.previousTrackCommand.addTarget(self, action: #selector(remoteCommandPrevious))
        commandCenter.nextTrackCommand.addTarget(self, action: #selector(remoteCommandNext))
    }
    
    @objc
    fileprivate func changePlayback(event: MPChangePlaybackPositionCommandEvent) -> MPRemoteCommandHandlerStatus {
        let seconds = event.positionTime
        player.seekTo(seconds)
        return .success
    }
    
    @objc
    fileprivate func remoteCommandPause() -> MPRemoteCommandHandlerStatus {
        player.pause()
        return .success
    }
    
    @objc
    fileprivate func remoteCommandPlay() -> MPRemoteCommandHandlerStatus {
        player.play()
        return .success
    }
    
    @objc
    fileprivate func remoteCommandNext() -> MPRemoteCommandHandlerStatus {
        player.next()
        return .success
    }
    
    @objc
    fileprivate func remoteCommandPrevious() -> MPRemoteCommandHandlerStatus {
        player.previous()
        return .success
    }
    
    //MARK: - Public Methods
    
    func setNowPlayingInfo(
        artworkImage: UIImage?,
        currentTime: Double,
        rate: Float = 1.0
    ) {
        guard let audio = player.currentAudio else { return }
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [
            MPMediaItemPropertyPlaybackDuration: audio.durationInSeconds,
            MPMediaItemPropertyTitle: audio.title,
            MPNowPlayingInfoPropertyElapsedPlaybackTime: currentTime,
            MPNowPlayingInfoPropertyPlaybackRate: rate,
            MPMediaItemPropertyArtwork: MPMediaItemArtwork(
                boundsSize: artworkSize,
                requestHandler: { size in resize(image: artworkImage, size: size) }
            )
        ]
    }
}
