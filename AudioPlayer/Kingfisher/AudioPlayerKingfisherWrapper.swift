//
//  AudioPlayerKingfisherWrapper.swift
//  AudioPlayer
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import UIKit
import Kingfisher

class AudioPlayerKingfisherWrapper {
    static func handle(
        path: String,
        width: Int,
        height: Int,
        imageView: UIImageView
    ) {

        var options: KingfisherOptionsInfo = [.backgroundDecode, .scaleFactor(0.8)]
            let resizeProcessor = ResizingImageProcessor(referenceSize: CGSize(width: width, height: height), mode: .aspectFill)
            options.append(.processor(resizeProcessor))
    
        let provider = Mp3ImageProvider(filePath: path)

        imageView.kf.setImage(with: provider, options: options)
    }
}
