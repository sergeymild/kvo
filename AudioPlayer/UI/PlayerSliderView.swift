//
//  PlayerSliderView.swift
//  FileManager
//
//  Created by Sergei on 01/05/2018.
//  Copyright © 2018 Sergey Golishnikov. All rights reserved.
//

import UIKit

public class PlayerSliderView: UISlider {
    var currentEvent: UIControl.Event? = nil
    var currentPosition: Float = 0
    
    public var thumbColor: UIColor! {
        didSet {
            setThumbImage(getColoredThumbImage(color: thumbColor, size: 0), for: .normal)
            setThumbImage(getColoredThumbImage(color: thumbColor, size: 0), for: .highlighted)
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        minimumTrackTintColor = Asset.Colors.seekBarPrimary.color
        maximumTrackTintColor = Asset.Colors.seekBarSecondary.color
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var newBounds = super.trackRect(forBounds: bounds)
        newBounds.size.height = 3
        return newBounds
    }
    
    public override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var bounds: CGRect = self.bounds
        bounds = bounds.insetBy(dx: -10, dy: -15)
        return bounds.contains(point)
    }
    
    
    fileprivate func getColoredThumbImage(color: UIColor, size: CGFloat) -> UIImage {
        
        //we will make circle with this diameter
        
        //circle will be created from UIView
        let circle = UIView(frame: CGRect(x: 0, y: 0, width: size, height: size))
        circle.setBackgroundColor(\.seekBarSecondary)
        circle.clipsToBounds = true
        circle.isOpaque = false
        
        //in the layer we add corner radius to make it circle and add shadow
        circle.layer.cornerRadius = size / 2
        
        //we add circle to a view, that is bigger than circle so we have extra 10 points for the shadow
        let view = UIView(frame: CGRect(x: 0, y: 0, width: size + 10, height: size + 10))
        view.backgroundColor = UIColor.clear
        view.addSubview(circle)
        
        circle.center = view.center
        
        //here we are rendering view to image, so we can use it later
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
