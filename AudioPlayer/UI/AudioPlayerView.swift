//
//  AudioPlayerView.swift
//  AudioPlayer
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import UIKit
import AVKit
import PinLayout

class AudioPlayerView: UICollectionViewCell {
    
    let coverImageView: CoverImageView = {
        let view = CoverImageView()
        view.cornerRadius = 10
        view.coverBackgroundView.setBackgroundColor(\.audioIconBackground)
        return view
    }()
    
    let timeSliderView: PlayerSliderView = {
        let view = PlayerSliderView()
        return view
    }()
    
    let currentTimeLabel: UILabel = {
        let view = UILabel()
        view.text = "00:00"
        view.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        view.setTextColor(\.audioSecondaryText)
        return view
    }()
    
    let totalTimeLabel: UILabel = {
        let view = UILabel()
        view.text = "00:00"
        view.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        view.setTextColor(\.audioSecondaryText)
        view.textAlignment = .right
        return view
    }()
    
    let titleLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 3
        view.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        view.textAlignment = .center
        view.setTextColor(\.audioPrimaryText)
        return view
    }()
    
    let controlsView = UIView()
    let replay10Button = ControlUIButtonView()
    let previousButton = ControlUIButtonView()
    let nextButton = ControlUIButtonView()
    let forward30Button = ControlUIButtonView()
    let playPauseButton = PlayPauseButtonView()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        debugPrint("prepare for reuse")
        playPauseButton.dispose()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        debugPrint("player init")
        timeSliderView.maximumTrackTintColor = Asset.Colors.seekBarPrimary.color
        timeSliderView.minimumTrackTintColor = Asset.Colors.seekBarSecondary.color
        timeSliderView.thumbColor = Asset.Colors.seekBarSecondary.color
        
        replay10Button.iconImageView.setImage(\.icReplay10, tint: \.audioIcon)
        forward30Button.iconImageView.setImage(\.icForward30, tint: \.audioIcon)
        previousButton.iconImageView.setImage(\.icPrevious, tint: \.audioIcon)
        nextButton.iconImageView.setImage(\.icNext, tint: \.audioIcon)
        playPauseButton.iconImageView.setTintColor(\.audioIcon)
        
        
        controlsView.addSubview(replay10Button)
        controlsView.addSubview(previousButton)
        controlsView.addSubview(playPauseButton)
        controlsView.addSubview(nextButton)
        controlsView.addSubview(forward30Button)

        addSubview(coverImageView)
        addSubview(timeSliderView)
        addSubview(currentTimeLabel)
        addSubview(totalTimeLabel)
        addSubview(titleLabel)
        addSubview(controlsView)
        
        playPauseButton.didTouchEnd = AudioPlayer.shared.togglePlaying
        previousButton.didTouchEnd = AudioPlayer.shared.previous
        nextButton.didTouchEnd = AudioPlayer.shared.next
        
        timeSliderView.addTarget(
            self,
            action: #selector(sliderTouchBegan(sender:)),
            for: .touchDown
        )

        timeSliderView.addTarget(
            self,
            action: #selector(sliderValueChanged(sender:)),
            for: .valueChanged
        )

        timeSliderView.addTarget(
            self,
            action: #selector(sliderTouchEnd(sender:)),
            for: [.touchUpInside, .touchCancel, .touchUpOutside]
        )
        

        replay10Button.didTouchEnd = {
            let current = AudioPlayer.shared.getCurrentTime() - 10
            AudioPlayer.shared.seekTo(current)
        }
        
        forward30Button.didTouchEnd = {
            let current = AudioPlayer.shared.getCurrentTime() + 30
            AudioPlayer.shared.seekTo(current)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        portraitOrientation()

        currentTimeLabel.pin
            .height(15)
            .left(to: timeSliderView.edge.left)
            .right(to: timeSliderView.edge.right)
            .above(of: timeSliderView)
            .marginBottom(-4)

        totalTimeLabel.pin
            .height(15)
            .left(to: timeSliderView.edge.left)
            .right(to: timeSliderView.edge.right)
            .above(of: timeSliderView)
            .marginBottom(-4)
    }
    
    private func portraitOrientation() {
        let margin = (frame.width - 64 - (44 * 4) - 72) / 5
        let coverSize: CGFloat = 159
        coverImageView.pin.hCenter().size(coverSize).top(68)
        timeSliderView.pin
            .left(16)
            .right(16)
            .below(of: coverImageView)
            .marginTop(32)

        layoutTitle()

        controlsView.pin
            .left(32)
            .right(32)
            .height(72)
            .bottom(56 + UIApplication.shared.windows[0].pin.safeArea.bottom)
        
        replay10Button.pin.size(38).left().vCenter()
        previousButton.pin.size(44).after(of: replay10Button).vCenter().marginLeft(margin)
        
        playPauseButton.pin.size(72).hCenter()
        
        forward30Button.pin.size(38).right().vCenter()
        nextButton.pin.size(44).before(of: forward30Button).vCenter().marginRight(margin)
    }
    
    private func layoutTitle() {
        titleLabel.textAlignment = .center
        titleLabel.pin
            .left(32)
            .right(32)
            .below(of: timeSliderView)
            .marginTop(50)
            .sizeToFit(.width)
    }
    
    func bind(_ model: AudioModel?) {
        guard let model = model else { return }
        timeSliderView.minimumValue = 0
        timeSliderView.maximumValue = Float(model.durationInSeconds)
        playPauseButton.isPlaying = false
        let time = AudioPlayer.shared.getCurrentTime()
        audioDidChangeTime(model: model, time: time)
        titleLabel.text = model.title
        layoutTitle()
        
        
        coverImageView.coverIconView.setImage(\.icAudio)
        coverImageView.coverIconView.setTintColor(\.audioPlaceholderIcon)

        let size = Int(UIScreen.main.bounds.width * 0.65)
        AudioPlayerKingfisherWrapper.handle(
            path: model.path,
            width: size,
            height: size,
            imageView: coverImageView.coverImage)
    }
    
    func audioDidChangeTime(model: AudioModel, time: Double) {
        timeSliderView.value = Float(time)
        currentTimeLabel.text = Int(time).formattedDuration
        totalTimeLabel.text = Int(model.durationInSeconds).formattedDuration
    }
    
    @objc
    func sliderTouchBegan(sender: UISlider) {
        AudioPlayer.shared.pause()
        currentTimeLabel.text = Int(timeSliderView.value).formattedDuration
        playPauseButton.isPlaying = false    
    }
    
    @objc
    func sliderValueChanged(sender: UISlider) {
        guard let model = AudioPlayer.shared.currentAudio else { return }
        currentTimeLabel.text = Int(sender.value).formattedDuration
        totalTimeLabel.text =
            (Int(model.durationInSeconds) - Int(sender.value)).formattedDuration
    }
    
    @objc
    func sliderTouchEnd(sender: UISlider) {
        AudioPlayer.shared.seekTo(Double(timeSliderView.value))
        playPauseButton.isPlaying = true
        AudioPlayer.shared.play()
    }
}
