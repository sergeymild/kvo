//
//  AudioPlayerViewCellHolder.swift
//  AudioPlayer
//
//  Created by Sergei Golishnikov on 26/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import Foundation
import UIKit

class AudioPlayerViewCellHolder: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func bind(playerView: UIView) {
        subviews.forEach { $0.removeFromSuperview() }
        addSubview(playerView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        subviews.forEach { $0.pin.all() }
    }
}
