//
//  SwipeToDismissView.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 15/10/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import UIKit

class SwipeToDismissView: UIView {
    enum PanFinishDirection {
        case left
        case right
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(swipedDown(recognizer:)))
        addGestureRecognizer(panGestureRecognizer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc
    func swipedDown(recognizer:UIPanGestureRecognizer) {
        switch recognizer.state {
        case .changed:
            let translation = recognizer.translation(in: self)
            transform = CGAffineTransform(translationX: max(0, translation.x), y: 0)
            self.alpha = 1 - abs(translation.x) / bounds.width
            
        case .ended:
            let velocity = recognizer.velocity(in: self)
            let translation = recognizer.translation(in: self)
            if !isGestureFinished(velocity: velocity, translation: translation) {
                cancelAnimation()
                return
            }
            
            
            let position = superview?.bounds.height ?? 0
            let distance = position - translation.x
            let springVelocity = velocity.x / distance
            let springDamping: CGFloat = 1
            
            UIView.animate(
                withDuration: 0.7,
                delay: 0,
                usingSpringWithDamping: springDamping,
                initialSpringVelocity: springVelocity,
                options: [],
                animations: { [weak self] in
                    self?.transform = CGAffineTransform(translationX: position, y: 0)
                    self?.alpha = 0
                }, completion: { [weak self] _ in self?.didViewDismiss() })
            
            
        default:
            cancelAnimation()
        }
    }
    
    func isGestureFinished(velocity: CGPoint, translation: CGPoint) -> Bool {
    
        if velocity.x > 100 {
            return true
        } else if translation.x > frame.width / 2 {
            return true
        }
        
        return false
    }
    
    func cancelAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.transform = .identity
            //self.alpha = 1
        })
    }
    
    func didViewDismiss() {}
}
