//
//  PlayPauseButtonView.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 05/03/2019.
//  Copyright © 2019 Sergei Golishnikov. All rights reserved.
//

import UIKit
import Logs
import KVO

class PlayPauseButtonView: UIView {
    var bag = DisposeKey()
    var isPlayingObserver: String?
    var didTouchEnd: (() -> Void)? = nil
    var isPlaying: Bool = false
    var isEndBeganAnimation = true
    var isTouchEnd = true
    
    let iconImageView: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(iconImageView)
        iconImageView.setTintColor(\.audioIcon)
        
        let isPlaying = AudioPlayer.shared.isPlaying
        iconImageView.setImage(isPlaying ? \.icPause : \.icPlay)
        AudioPlayer.shared.kvo.valueObserve(\.isPlaying) {
            [weak self] (isPlaying) in
            self?.iconImageView.setImage(isPlaying ? \.icPause : \.icPlay)
        }.disposed(by: bag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        iconImageView.pin.vCenter().size(width * 0.9)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        isTouchEnd = false
        isEndBeganAnimation = false
        UIView.animate(withDuration: 0.15, animations: { [weak self] in
            self?.iconImageView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }, completion: { [weak self] _ in
            if self?.isTouchEnd == true {
                self?.scaleToNormal()
            }
            self?.isEndBeganAnimation = true
        })
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isEndBeganAnimation {
            scaleToNormal()
        }
        isTouchEnd = true
        didTouchEnd?()
    }
    
    fileprivate func scaleToNormal() {
        UIView.animate(withDuration: 0.15, animations: { [weak self] in
            //self.isPlaying = !self.isPlaying
            self?.iconImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { [weak self] _ in
            self?.isEndBeganAnimation = true
        })
    }
    
    func dispose() {
        didTouchEnd = nil
        bag = DisposeKey()
    }
}
