

Pod::Spec.new do |spec|

spec.name         = "KVO"
spec.version      = "0.0.1"
spec.summary      = "KVO"

spec.description  = <<-DESC
  KVO library
                   DESC

spec.homepage     = "http://github.com"

spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }
spec.author       = { "sergeymild" => "jean.timex@gmail.com" }
spec.source       = { git: "https://sergeymild@bitbucket.org/sergeymild/swiftlogs.git", tag: "master" }
spec.swift_version = "5.1"
spec.ios.deployment_target = "11.4"
spec.source_files  = "KVO/**/*.{h,m,swift}"
end

