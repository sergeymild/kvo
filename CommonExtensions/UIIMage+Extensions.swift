//
//  UIIMage+Extensions.swift
//  CommonExtensions
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import UIKit

public extension UIImage {
    func createScaled(size: CGSize) -> UIImage {
        let imageRenderer = UIGraphicsImageRenderer(size: size)
        return imageRenderer.image(actions: { (context) in
            draw(in: CGRect(origin: .zero, size: size))
        })
    }
    
    func withBackground(color: UIColor, size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, true, 0)

        guard let ctx = UIGraphicsGetCurrentContext() else { return self }
        defer { UIGraphicsEndImageContext() }

        let rect = CGRect(origin: .zero, size: size)
        ctx.setFillColor(color.cgColor)
        ctx.fill(rect)
        ctx.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height))
        ctx.draw(cgImage!, in: CGRect(x: size.width / 2 - self.size.width / 2,
        y: size.height / 2 - self.size.height / 2,
        width: self.size.width,
        height: self.size.height))

        return UIGraphicsGetImageFromCurrentImageContext() ?? self
    }
}
