//
//  CommonExtensions.h
//  CommonExtensions
//
//  Created by Sergei Golishnikov on 24/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for CommonExtensions.
FOUNDATION_EXPORT double CommonExtensionsVersionNumber;

//! Project version string for CommonExtensions.
FOUNDATION_EXPORT const unsigned char CommonExtensionsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CommonExtensions/PublicHeader.h>


