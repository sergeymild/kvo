//
//  String+Extensions.swift
//  FileManager
//
//  Created by Sergei Golishnikov on 24/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import Foundation

public extension String {
    var ext: String {
        return (self as NSString).pathExtension
    }
    
    var nameWithExtension: String {
        ((self as NSString).lastPathComponent)
    }
    
    var nameWithoutExtension: String {
        ((self as NSString).lastPathComponent).withoutExtension
    }
    
    var withoutExtension: String {
        return (self as NSString).deletingPathExtension.deletingSuffix(".")
    }
    
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else {
            return self
        }
        return String(self.dropFirst(prefix.count))
    }

    func deletingSuffix(_ suffix: String) -> String {
        guard self.hasSuffix(suffix) else {
            return self
        }
        return String(self.dropLast(suffix.count))
    }
}
