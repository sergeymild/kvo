//
//  UIView+Extensions.swift
//  CommonExtensions
//
//  Created by Sergei Golishnikov on 25/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import UIKit


public extension UIView {
    func shadow(
        color: UIColor,
        opacity: Float = 0.3,
        radius: CGFloat = 10,
        width: Int = 0,
        height: Int = 0
    ) {
        self.clipsToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowRadius = radius
    }
}
