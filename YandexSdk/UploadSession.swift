//
//  UploadSession.swift
//  YandexSdk
//
//  Created by Sergei Golishnikov on 24/03/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import Foundation

typealias UploadSessionProgress = (Float, Float) -> Void
typealias UploadCompetionHandler = (Error?) -> Void

class UploadSession: NSObject, URLSessionTaskDelegate {
    
    var uploadProgress: UploadSessionProgress? = nil
    var completionHandler: UploadCompetionHandler? = nil
    
    public lazy var session : URLSession = {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.httpShouldUsePipelining = true
        let _session = URLSession(
            configuration: sessionConfig,
            delegate: self,
            delegateQueue: OperationQueue.main
        )
        return _session
    }()
    
    var sessionDataTask: URLSessionUploadTask? = nil
    
    open func uploadTask(
        with url: String,
        fromFile fileURL: URL,
        method: String = "POST",
        headers: [String: String]
    ) -> URLSessionUploadTask? {
        var request = URLRequest(url: URL(string: url)!)
        for header in headers {
            request.addValue(header.value, forHTTPHeaderField: header.key)
        }
        
        sessionDataTask = session.uploadTask(
            with: request,
            fromFile: fileURL
        )
        sessionDataTask?.resume()
        return sessionDataTask
    }
    
    func urlSession(
        _ session: URLSession,
        task: URLSessionTask,
        didSendBodyData bytesSent: Int64,
        totalBytesSent: Int64,
        totalBytesExpectedToSend: Int64
    ) {
        uploadProgress?(Float(totalBytesSent), Float(totalBytesExpectedToSend))
    }
    
    func urlSession(
        _ session: URLSession,
        task: URLSessionTask,
        didCompleteWithError error: Error?
    ) {
        completionHandler?(error)
        completionHandler = nil
        uploadProgress = nil
    }
}
